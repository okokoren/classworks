#include <iostream>

int main()
{
	/* bugs list:
	* 1. we  were trying to chack if it equels to 0 and because it is an unsinged int it can go below 1 
	* 2. the program can run but it is not efficiant to declare a variable every time the loop run and if it is runnin infinitly
	* it could cause an unneccesery problems
	*/
	unsigned int size = 0;

	int t1 = 0, t2 = 1;
	int nextTerm = 0;

	std::cout << "what is the size of the series? ";
	std::cin >> size;

	while (size > 0)
	{
		std::cout << t1 << ", ";

		nextTerm = t1 + t2;
		t1 = t2;
		t2 = nextTerm;

		size--;
	}

	return 0;
}