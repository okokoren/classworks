#include "shape.h"
#include "triangle.h"
#include <iostream>


int main() 
{
	/*bugs:
	* 1. shape and the triangle didnt have the same signature for the method get_area so the compiler took them as two diffrent
	* functions
	*/
	Triangle triangle(2, 10);
	std::cout << "The area of the triangle is " << triangle.get_area() << std::endl;

	Shape* pTriangle = new Triangle(2, 10);
	std::cout << "The area of the triangle is " << pTriangle->get_area() << std::endl;
	delete pTriangle;

	return 0;
}
