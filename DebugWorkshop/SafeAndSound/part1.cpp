#include "part1.h"
#include <iostream>

char* string_copy(char* dest, unsigned int destsize, char* src)
{
	/* the bug was that i am not putting a null char at the end of the string so if the string is shorter ther is an ovelflow*/
	char* ret = dest;
	unsigned int i = destsize;
	while (i)
	{
		*dest++ = *src++;
		i--;
	}
	 ret[destsize - 1] = 0;

	return ret;
}

void part1()
{
	char password[] = "secret";
	char dest[12] = "";
	char src[] = "hello world!";

	 string_copy(dest, 12, src);

	std::cout << src << std::endl;
	std::cout << dest << std::endl;
}
